// Copyright 2022 Tobias Alexandra Platen
// SPDX-License-Identifier: Apache-2.0

#pragma once

#include <emmintrin.h>
#include <xmmintrin.h>
//there is no immintrin.h on ppc64el

//TODO: implement for POWER9 and libre-soc

/* Dummy defines for floating point control */
#define _MM_MASK_MASK 0x1f80
#define _MM_MASK_DIV_ZERO 0x200
#define _MM_FLUSH_ZERO_ON 0x8000
#define _MM_MASK_DENORM 0x100
#define _MM_SET_EXCEPTION_MASK(x)
#define _MM_SET_FLUSH_ZERO_MODE(x)

__forceinline int _mm_getcsr()
{
  return 0;
}

#define _MM_SET_EXCEPTION_MASK(x)

__forceinline void _mm_setcsr(unsigned int)
{
}

